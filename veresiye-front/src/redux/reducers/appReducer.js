import {ADD_CREDIT, ADD_DEPT, CREATE_ACCOUNT, DELETE_ACCOUNT, GET_APP_DATA, UPDATE_CREDIT} from "../actions";

const initialState = {
    accounts:[],
}
export const appReducer = (state=initialState,action) => {
    switch (action.type) {
        case GET_APP_DATA:
    console.log(action.payload.accounts)
            return {
                ...state,
                accounts: action.payload.accounts
            };
        case CREATE_ACCOUNT:
            return {
                ...state,
                accounts:[...state.accounts,action.payload]
            };

        case DELETE_ACCOUNT:

            return {
                ...state,
                accounts:state.accounts.filter(item=>{
                    return item.id !== action.payload
                })
            };

        case ADD_CREDIT:
            return {
                ...state,
                accounts:state.accounts.map(e=>{
                    if(e.id === action.payload.account_id){
                        return {
                            ...e,totalcredit:e.totalcredit+action.payload.data.total,balance:e.balance+action.payload.data.total,credits:[...e.credits,action.payload.data]

                        }
                    }else{
                        return e
                    }
                })
            };
        case ADD_DEPT:
            return {
                ...state,
                accounts:state.accounts.map(e=>{
                    if (e.id === action.payload.account_id){
                        return {
                            ...e,totaldept:e.totaldept+action.payload.data.total,balance:e.balance-action.payload.data.total,depts:[...e.depts,action.payload.data]
                        }
                    }else{
                        return e
                    }
                })
            };
        case UPDATE_CREDIT:

            return {
                ...state,
                accounts:state.accounts.map(e=>{
                    if(e.id===action.payload.account_id){
                        return {
                            ...e,totalcredit:action.payload.data
                        }
                    }else{
                        console.log('yok')
                        return e
                    }
                })
            }

            /*let oldState = state;
                let index = oldState.accounts.findIndex(item=>item.id===action.payload.id);
                if (index !== -1){
                    oldState[index].totalcredit=action.payload.data;
                    return {
                        ...state,
                        accounts:oldState
                    }
                }*/
        default:
            return state

    }
};

