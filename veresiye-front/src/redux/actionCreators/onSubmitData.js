import axios from 'axios';
import {createAccount} from "../actions/accountActions";

/*const setData = async (dispatch,data) => {
    let newData={
        id:data.data.id,
        name:data.data.name,
        person_name:data.data.person_name,
        tel:data.data.tel,
        address:data.data.address
    };
    await axios.post('http://localhost:8000/api/depts',{
        account_id:data.data.id,
        total:0
    })
        .then(data=> newData={...newData,totaldept:data.data.total})
    await axios.post('http://localhost:8000/api/credits',{
        account_id:data.data.id,
        total:0
    })
        .then(data=> newData={...newData,totalcredit:data.data.total})

    dispatch(createAccount(newData))

}*/

export const onSubmitData = (account) => {
    return dispatch => {
            return axios.post('http://localhost:8000/api/accounts', {
            name: account.name,
            person_name: account.person_name,
            tel: account.tel,
            address: account.address
        })
            .then(data =>{
                dispatch(createAccount(data.data))
            })
    }
}