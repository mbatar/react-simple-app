import React, {Component} from 'react';
import {Card, CardHeader, CardBody, CardText, Col, Row, Button} from 'reactstrap';
import {connect} from "react-redux";
import AddCredit from "./AddCredit";
import AddDept from "./AddDept";
import {bindActionCreators} from "redux";
import {onCreateCredit} from "../redux/actionCreators/onCreateCredit";
import {onCreateDept} from "../redux/actionCreators/onCreateDept";
import {onDeleteAccount} from "../redux/actionCreators/onDeleteAccount";
import ShowInfo from "./ShowInfo";

class Account extends Component {

        state = {
            isChecked: false,
            addCredit: false,
            addDept: false,
            showInfo: false,

        }


    changeChecked = () => {
        this.setState({isChecked:!this.state.isChecked})
    }

    changeRender = (e) => {

        switch (e.target.value) {
            case 'credit':
                this.setState({addCredit: !this.state.addCredit})
                break;
            case 'dept':
                this.setState({addDept: !this.state.addDept})
                break;
            default:
                console.log('error');
                break
        }
    }

    showInfo = () => {
            this.setState({showInfo: !this.state.showInfo})
    }

    deleteAccount = () => {
            this.props.onDeleteAccount(this.props.item.id)
    }

    render() {
        return (
            <div>
                <Card className="my-3" style={{border: '3px solid #FF8C00'}}>
                    <CardHeader className="d-flex justify-content-between">
                        <h4 onClick={this.changeChecked}
                            className="d-inline-block my-auto" style={{cursor: "pointer"}}>{this.props.item.name}</h4>
                        <div>
                            <i onClick={this.showInfo} className="fas fa-search my-auto mr-2" style={{cursor: "pointer"}}/>
                            <i onClick={this.deleteAccount} className="far fa-trash-alt my-auto" style={{cursor: "pointer"}}/>
                        </div>
                    </CardHeader>
                    <CardBody className={this.state.isChecked ? 'd-block' : 'd-none'}>
                        {
                            this.state.addCredit ? (
                                <AddCredit
                                    changeRender={this.changeRender}
                                    account_id={this.props.item.id}
                                />
                            ) : this.state.addDept ? (
                                <AddDept
                                    changeRender={this.changeRender}
                                    account_id={this.props.item.id}
                                />
                                ) : this.state.showInfo ? (
                                    <ShowInfo
                                        showInfo={this.showInfo}
                                        account_id={this.props.item.id}
                                        account_name={this.props.item.name}
                                        credits={this.props.item.credits}
                                        depts={this.props.item.depts}
                                    />
                                )
                                 : (
                                    <div>
                                        <CardText>Yetkili Kişi: {this.props.item.person_name} </CardText>
                                        <CardText>Tel: {this.props.item.tel}</CardText>
                                        <CardText>Adres: {this.props.item.address}</CardText>
                                        <CardText>Borç Toplamı: {this.props.item.totaldept} TL</CardText>
                                        <CardText>Alacak Toplamı: {this.props.item.totalcredit} TL</CardText>
                                        <CardText>Bakiye: {this.props.item.balance} TL</CardText>
                                        <Row className="mt-4">
                                            <Col>
                                                <Button value="credit" className="btn btn-success w-100" onClick={this.changeRender}>ALACAK EKLE</Button>
                                            </Col>
                                            <Col>
                                                <Button value="dept" className="btn btn-danger w-100" onClick={this.changeRender}>BORÇ EKLE</Button>
                                            </Col>
                                        </Row>
                                    </div>
                                )
                        }
                    </CardBody>
                </Card>
            </div>
        );
    }
}





const mapStateToProps = state => {
    return state
}
const mapDispatchToProps = dispatch => bindActionCreators({
    onCreateCredit,
    onCreateDept,
    onDeleteAccount
},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(Account);