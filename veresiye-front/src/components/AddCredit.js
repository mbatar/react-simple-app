import React, {Component} from 'react';
import {Button, FormGroup, Input, Label} from "reactstrap";
import {bindActionCreators} from "redux";
import {onCreateCredit} from "../redux/actionCreators/onCreateCredit";
import {connect} from "react-redux";

class AddCredit extends Component {

    state={
            total:0,
            description:'',
            account_id:this.props.account_id
    }

    onChangeHandler = (e) => {
        const target = e.target;
        const value = target.type === 'number' ? Number(target.value) : target.value
        this.setState({
            [e.target.name]:value
        })
    }

    onSubmitCredit = (e)=>{
    e.persist()
        this.props.onCreateCredit(this.state)
            .then(()=> this.props.changeRender(e))
    }

    render() {
        return (
            <div>
                <FormGroup>
                    <Label className="font-weight-bold" for="description">Alacak Açıklaması</Label>
                    <Input onChange={this.onChangeHandler} style={{border:'3px solid #FF8C00'}}  type="textarea" name="description" id="description" placeholder="Lütfen Doldurunuz. Ör: (Boya Aldı)" />
                </FormGroup>
                <FormGroup >
                    <Label className="font-weight-bold" for="total">Tutar</Label>
                    <Input onChange={this.onChangeHandler} style={{border:'3px solid #FF8C00'}}  type="number"  name="total" id="total" placeholder="Lütfen Doldurunuz. Ör: (123)" />
                </FormGroup>
                <Button onClick={this.onSubmitCredit} value="credit" className="btn btn-success w-100" >ONAYLA</Button>
            </div>
        )
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({
    onCreateCredit
},dispatch)
const mapStateToProps = state => {
    return state
}
export default connect(mapStateToProps,mapDispatchToProps)(AddCredit)