import React, {Component} from 'react';
import {Button, FormGroup, Input} from "reactstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {onUpdateCredit} from "../redux/actionCreators/onUpdateCredit";


class UpdateCredit extends Component {
    state = {
        total: 0
    }

    onChangeHandler = (e) => {
    let number = Number(e.target.value)
        this.setState({
            [e.target.name]:number
        })
    }

    updateCredit = () => {
        console.log(this.state.total)
        this.props.onUpdateCredit(this.props.id,this.state.total)
    }

    render() {
        return (
            <div>
                <FormGroup>

                    <Input type="number" className="border border-success" placeholder="Alacak Miktarı Giriniz" name="total" onChange={this.onChangeHandler}/>
                    <Button className="btn btn-success w-100" onClick={this.updateCredit}>ALACAK EKLE</Button>
                </FormGroup>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return state
}

const mapDispatchToProps = dispatch => bindActionCreators({
    onUpdateCredit
},dispatch)

export default connect(mapStateToProps,mapDispatchToProps)(UpdateCredit)