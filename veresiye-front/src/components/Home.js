import React, {Component} from 'react';
import {connect} from "react-redux";
import {onGetAllData} from "../redux/actionCreators/onGetAllData";
import {bindActionCreators} from "redux";
class Home extends Component {


    render() {
        return (
            <div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return state
}

const mapDispatchToProps = dispatch =>bindActionCreators({
    onGetAllData
},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(Home);