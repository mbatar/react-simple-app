import React, {Component} from 'react';
import Navi from "./Navi";
import {Route,Switch} from 'react-router-dom'
import Home from "./Home";
import AddAccount from "./AddAccount";
import AccountList from "./AccountList";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {onGetAllData} from "../redux/actionCreators/onGetAllData";

class App extends Component {
    componentDidMount() {
        this.props.onGetAllData()
    }
    render() {
        return (
            <div style={{backgroundColor:'#FF8C00'}}>
                <Navi/>
                <div className="mt-3 bg-info p-3">
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/accounts" component={AccountList}/>
                        <Route exact path="/addAccount" component={AddAccount} />
                    </Switch>
                </div>
            </div>
        )
    }

}
const mapStateToProps = state => {
    return state
}
const mapDispatchToProps = dispatch =>bindActionCreators({
    onGetAllData
},dispatch)
export default connect(mapStateToProps,mapDispatchToProps)(App);
