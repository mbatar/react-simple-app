import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {onGetInfo} from "../redux/actionCreators/onGetInfo";
import {Table} from 'reactstrap'
import {connect} from "react-redux";

class ShowInfo extends Component {
    componentDidMount() {
        //this.props.onGetInfo(this.props.account_id)
        console.log(this.props.credits);
        console.log(this.props.depts);
    }

    render() {
        return (
            <div>
                <h2 className="mb-3">Alacak Hareketleri</h2>
                <Table className="mb-5" bordered hover>
                    <thead>
                    <tr>
                        <th>Açıklama</th>
                        <th>Tutar</th>
                        <th>Oluşturulma Zamanı</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.credits.map((item,index) => {
                        return (
                            <tr key={index}>
                                <td>{item.description}</td>
                                <td>{item.total} TL</td>
                                <td>{item.created_at}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
                <h2 className="mb-3">Borç Hareketleri</h2>
                <Table bordered hover>
                    <thead>
                    <tr>
                        <th>Açıklama</th>
                        <th>Tutar</th>
                        <th>Oluşturulma Zamanı</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.depts.map((item,index) => {
                        return (
                            <tr key={index}>
                                <td>{item.description}</td>
                                <td>{item.total} TL</td>
                                <td>{item.created_at}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return state;
}
const mapDispatchToProps = dispacth => bindActionCreators({
    onGetInfo
},dispacth)
export default connect(mapStateToProps,mapDispatchToProps)(ShowInfo) ;