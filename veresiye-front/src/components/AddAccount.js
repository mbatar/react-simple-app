import React, {Component} from 'react';
import {connect} from "react-redux";
import {Form,FormGroup,Label,Input,Button} from 'reactstrap';
import {bindActionCreators} from "redux";
import {onSubmitData} from "../redux/actionCreators/onSubmitData";
class AddAccount extends Component {
    state={
        name:'',
        person_name:'',
        tel:'',
        address:'',
    }

    onChangeHandler = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    createAccount = () => {
        this.props.onSubmitData(this.state)
            .then(()=>this.props.history.push('/accounts'))

    }

    render() {
        return (
            <div>
                <h1 className="text-white">Hesap Ekle</h1>
                <Form >
                    <FormGroup>
                        <Label className="text-white font-weight-bold" for="name">Şirket İsmi</Label>
                        <Input style={{border:'3px solid #FF8C00'}} onChange={this.onChangeHandler} type="text" name="name" id="name" placeholder="Lütfen Doldurunuz. Ör: (Güneş İnşaat)" />
                    </FormGroup>
                    <FormGroup>
                        <Label className="text-white font-weight-bold" for="person_name">Yetkili Kişi</Label>
                        <Input style={{border:'3px solid #FF8C00'}} onChange={this.onChangeHandler} type="text" name="person_name" id="person_name" placeholder="Lütfen Doldurunuz. Ör: (Ahmet Güneş)" />
                    </FormGroup>
                    <FormGroup>
                        <Label className="text-white font-weight-bold" for="tel">Tel</Label>
                        <Input style={{border:'3px solid #FF8C00'}} onChange={this.onChangeHandler} type="number" name="tel" id="tel" placeholder="Lütfen Doldurunuz. Ör: (01112223344)" />
                    </FormGroup>
                    <FormGroup>
                        <Label className="text-white font-weight-bold" for="address">Adres</Label>
                        <Input style={{border:'3px solid #FF8C00'}} onChange={this.onChangeHandler} type="textarea" name="address" id="address" placeholder="Lütfen Doldurunuz. Ör: (Akçaabat/TRABZON)" />
                    </FormGroup>
                </Form>
                <Button style={{backgroundColor:'#FF8C00', fontWeight:'bold'}} onClick={this.createAccount}>EKLE</Button>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return state;
}

const mapDispatchToProps = dispatch => bindActionCreators({
    onSubmitData
},dispatch)

export default connect(mapStateToProps,mapDispatchToProps)(AddAccount);