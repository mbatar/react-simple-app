import React, {Component} from 'react';
import Account from "./Account";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {onGetAllData} from "../redux/actionCreators/onGetAllData";

class AccountList extends Component {

    render() {
        return (
            <div>
                <h1 className="text-white">Hesap Listesi</h1>
                {
                    this.props.appReducer.accounts.map((item,index)=>{
                        let account = {...item}
                        return(
                            <Account
                                item={account}
                                key={index}
                            />
                        )
                    })
                }
            </div>
        );
    }
}
const mapStateToProps = state => {
    return state
}
const mapDispatchToProps = dispatch => bindActionCreators({
    onGetAllData
},dispatch)

export default connect(mapStateToProps,mapDispatchToProps)(AccountList);