<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Deptmoney extends Model {

    protected $fillable = [
        'total','account_id','description'
    ];


}
